import { usePathname, useRouter, useSearchParams } from "next/navigation";

type TKeyValue = {
  key: string;
  value: string;
};
export const useParams = (allParam: string[]) => {
  const router = useRouter();
  const searchParams = useSearchParams();
  const pathName = usePathname();
  const makeKeyValueParam = ({ key, value }: TKeyValue) => {
    return key + "=" + value;
  };
  const mergerAlllParam = (allParam: string[]): string => {
    return allParam.reduce((accumulator, currentValue) => {
      return accumulator + "&" + currentValue;
    }, "");
  };
  const setUrlParam = ({ key, value }: TKeyValue): void => {
    const allParamString: string[] = [];
    allParam.map((keyParam) => {
      const valueParam = key == keyParam ? value : searchParams.get(keyParam);
      valueParam && allParamString.push(makeKeyValueParam({ key: keyParam, value: valueParam }));
    });

    const param = allParamString.length > 0 ? pathName + "?" + mergerAlllParam(allParamString) : pathName;

    router.push(param);

    return;
  };

  const getValueParams = (key: string): string | undefined => searchParams.get(key)?.toString();

  return { setUrlParam, getValueParams };
};
