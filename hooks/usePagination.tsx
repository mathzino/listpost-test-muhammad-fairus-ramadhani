import { TPaginationData } from "@/entities/entity";
import { useEffect, useState } from "react";
interface IUsePagination {
  totalPage: number;
  currentPage: number;
}
const usePagination = ({ totalPage, currentPage }: IUsePagination): TPaginationData => {
  let minPage = 1;
  let maxPage = totalPage;
  let paginationCount = 5;

  if (currentPage < 1 || currentPage > totalPage) return { currentPage: 1, pages: [1, 2, 3, 4, 5] };
  if (currentPage <= paginationCount) {
    minPage = 1;
    maxPage = 5;
  }
  if ((currentPage > paginationCount && currentPage + 2 <= totalPage) || (currentPage < minPage && currentPage - 2 >= 1)) {
    maxPage = currentPage + 2;
    minPage = currentPage - 2;
  }
  if (currentPage >= paginationCount) {
    minPage = currentPage - 4;
    maxPage = currentPage;
  }

  let newPages: number[] = [];
  for (let i = minPage; i <= maxPage; i++) {
    newPages.push(i);
  }
  const pages = newPages;

  return { currentPage, pages };
};

export default usePagination;
