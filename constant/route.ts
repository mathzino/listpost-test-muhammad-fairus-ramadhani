export const Route = {
  work: "/",
  about: "/about",
  services: "/services",

  ideas: "/ideas",
  careers: "/careers",
  contact: "/contact",
};
