import Parallax from "@/component/molecules/parallax";
import SelectOption from "@/component/molecules/selectOption";
import Card from "@/component/organism/card";
import Filter from "@/component/organism/filter";
import Pagination from "@/component/organism/pagination";
import { TCard } from "@/entities/entity";

type Props = {
  params: {};
  searchParams: { [key: string]: string | string[] | undefined };
};

export default async function Ideas(props: Props) {
  const pageSize = props.searchParams["page[size]"] || "10";
  const pageNumber = props.searchParams["page[number]"] || "1";
  const sort = props.searchParams["sort"] == "newest" ? "-published_at" : "published_at";

  const endpoint = `https://suitmedia-backend.suitdev.com/api/ideas?page[number]=${pageNumber}&page[size]=${pageSize}&append[]=small_image&append[]=medium_image&sort=${sort}`;

  const response = await fetch(endpoint, {
    headers: {
      Accept: "application/json",
    },
  });

  const getList = await response.json();
  const newsList = getList.data;
  const metadata = getList.meta;

  return (
    <div className="  bg-white flex justify-center py-16 flex-col  items-center ">
      <Parallax>
        <div className="max-w-7xl w-full flex flex-col gap-16 z-10">
          <Filter from={metadata.from} to={metadata.to} total={metadata.total} />
          <div className="  grid grid-cols-3 w-full  3 gap-y-8 justify-around">
            {newsList.map((news: TCard) => {
              return (
                <Card
                  key={news.id}
                  title={news.title}
                  imageUrl="https://images.unsplash.com/photo-1584432743501-7d5c27a39189?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8bmljZSUyMHZpZXd8ZW58MHx8MHx8fDA%3D"
                  published_at={news.published_at}
                  small_image={news.small_image}
                  medium_image={news.medium_image}
                />
              );
            })}
          </div>

          <div className=" flex justify-center">
            <Pagination metadata={metadata} />
          </div>
        </div>
      </Parallax>
    </div>
  );
}
