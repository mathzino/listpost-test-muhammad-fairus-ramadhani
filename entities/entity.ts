type TImage = {
  id: string;
  mime: string;
  url: string;
  file_name: string;
};

export type TCard = {
  title: string;
  imageUrl: string;
  id?: string | number;
  published_at: string;
  small_image: TImage[];
  medium_image: TImage[];
};

export type TPaginationData = {
  currentPage: number;
  pages: number[];
};
