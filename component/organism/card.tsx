import { TCard } from "@/entities/entity";
import React from "react";
import ImageLoader from "../molecules/imageLoader";
import { elipsis } from "@/utils/elipsis";
interface ICard extends TCard {}
export default function Card({ title, small_image, medium_image, published_at }: ICard) {
  const inputDate = new Date(published_at);

  const options: any = { day: "numeric", month: "long", year: "numeric" };
  const outputDateString = inputDate.toLocaleDateString("id-ID", options);
  return (
    <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow col-span-1">
      <ImageLoader imageSmallUrl={small_image[0]?.url} imageUrl={medium_image[0]?.url} />
      <div className="p-5">
        <p className=" text-gray-300 font-semibold">{outputDateString}</p>
        <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 text-ellipsis overflow-hidden ">{elipsis(title, 60)}</h5>
      </div>
    </div>
  );
}
