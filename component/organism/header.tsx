"use client";
import { FC, useEffect, useState } from "react";
import Menu, { TMenu } from "@/component/molecules/menu";
import { Route } from "@/constant/route";
import clsx from "clsx";
import Image from "next/image";
import suitmediaLogo from "@/public/suitmedia.jpeg";
import { useScroll } from "@/hooks/useScroll";

const listMenu: TMenu[] = [
  { label: "Work", value: Route.work },
  { label: "About", value: Route.about },
  { label: "Services", value: Route.services },
  { label: "Ideas", value: Route.ideas },
  { label: "Careers", value: Route.careers },
  { label: "Contact", value: Route.contact },
];

const Header: FC<any> = () => {
  const { prevScrollY, scrollDirection } = useScroll();

  return (
    <div className={clsx(" bg-orange-600 flex justify-center py-3 transition-all fixed w-full overflow-hidden z-30 ", scrollDirection == "down" && " opacity-0", scrollDirection == "up" && "opacity-80", prevScrollY == 0 && "opacity-100")}>
      <div className="flex justify-between items-center w-full max-w-7xl">
        <div>
          <Image src={suitmediaLogo} alt="tes" width={100} />
        </div>
        <Menu listMenu={listMenu} />
      </div>
    </div>
  );
};

export default Header;
