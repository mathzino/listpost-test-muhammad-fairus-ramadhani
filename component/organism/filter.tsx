"use client";
import React from "react";
import SelectOption from "../molecules/selectOption";
import { useParams } from "@/hooks/useParams";
import { keyParams } from "@/constant/filter";
const pageSizeOption = [
  { name: "10", value: 10 },
  { name: "20", value: 20 },
  { name: "50", value: 50 },
];
const sortOPtion = [{ name: "newest" }, { name: "longest" }];

interface IFilter {
  from: string;
  to: string;
  total: string;
}
export default function Filter({ from, to, total }: IFilter) {
  const { setUrlParam, getValueParams } = useParams(keyParams);

  return (
    <div className="   text-black flex justify-between ">
      <div>
        showing {from}-{to} of {total}
      </div>
      <div className=" flex gap-4">
        <div>
          <label htmlFor="size">Show per page : </label>
          <SelectOption
            options={pageSizeOption}
            name="page[size]"
            value={getValueParams("page[size]")}
            onChange={(e) => {
              setUrlParam({ key: "page[size]", value: e.target.value });
            }}
          />
        </div>
        <div>
          <label htmlFor="sort">Sort by :</label>
          <SelectOption
            options={sortOPtion}
            name="sort"
            value={getValueParams("sort")}
            onChange={(e) => {
              setUrlParam({ key: "sort", value: e.target.value });
            }}
          />
        </div>
      </div>
    </div>
  );
}
