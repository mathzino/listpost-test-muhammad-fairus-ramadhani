"use client";
import { TPaginationData } from "@/entities/entity";
import usePagination from "@/hooks/usePagination";

import React from "react";

import clsx from "clsx";
import { useParams } from "@/hooks/useParams";
import { keyParams } from "@/constant/filter";

export default function Pagination({ metadata }: any) {
  const { currentPage, pages }: TPaginationData = usePagination({ totalPage: metadata.last_page, currentPage: metadata.current_page });
  const { setUrlParam, getValueParams } = useParams(keyParams);

  return (
    <nav aria-label="Page navigation example">
      <ul className="flex items-center -space-x-px h-10 text-base">
        <li>
          <button
            onClick={() => {
              setUrlParam({ key: "page[number]", value: "1" });
            }}
            className="hover:bg-orange-400 hover:bg-opacity-70 flex items-center justify-center px-4 h-10 ms-0 leading-tight text-gray-500 bg-white border border-e-0 border-gray-300 rounded-s-lg     "
          >
            <svg className="w-3 h-3 rtl:rotate-180" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
              <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 1 1 5l4 4" />
            </svg>
            <svg className="w-3 h-3 rtl:rotate-180" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
              <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 1 1 5l4 4" />
            </svg>
          </button>
        </li>
        <li>
          <button
            onClick={() => {
              setUrlParam({ key: "page[number]", value: (currentPage - 1).toString() });
            }}
            className="hover:bg-orange-400 hover:bg-opacity-70 flex items-center justify-center px-4 h-10 leading-tight text-gray-500 bg-white border border-gray-300     "
          >
            <svg className="w-3 h-3 rtl:rotate-180" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
              <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 1 1 5l4 4" />
            </svg>
          </button>
        </li>
        {pages.map((page) => {
          return (
            <li key={page}>
              <button
                onClick={() => {
                  setUrlParam({ key: "page[number]", value: page.toString() });
                }}
                className={clsx(
                  "hover:bg-orange-400 hover:bg-opacity-70 flex items-center justify-center px-4 h-10 leading-tight text-gray-500  border border-gray-300",
                  getValueParams("page[number]") == page.toString() && " text-white bg-orange-600"
                )}
              >
                {page}
              </button>
            </li>
          );
        })}

        <li>
          <button
            onClick={() => {
              setUrlParam({ key: "page[number]", value: (currentPage + 1).toString() });
            }}
            className="hover:bg-orange-400 hover:bg-opacity-70 flex items-center justify-center px-4 h-10 leading-tight text-gray-500 bg-white border border-gray-300     "
          >
            <svg className="w-3 h-3 rtl:rotate-180" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
              <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m1 9 4-4-4-4" />
            </svg>
          </button>
        </li>
        <li>
          <button
            onClick={() => {
              setUrlParam({ key: "page[number]", value: metadata.last_page.toString() });
            }}
            className="hover:bg-orange-400 hover:bg-opacity-70 flex items-center justify-center px-4 h-10 leading-tight text-gray-500 bg-white border border-gray-300 rounded-e-lg     "
          >
            <svg className="w-3 h-3 rtl:rotate-180" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
              <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m1 9 4-4-4-4" />
            </svg>{" "}
            <svg className="w-3 h-3 rtl:rotate-180" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
              <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m1 9 4-4-4-4" />
            </svg>
          </button>
        </li>
      </ul>
    </nav>
  );
}
