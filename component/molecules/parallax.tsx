import React from "react";

export default function Parallax({ children }: { children: React.ReactNode }) {
  return (
    <div
      className="mx-auto h-full  w-screen overflow-y-scroll bg-cover bg-fixed bg-center bg-no-repeat shadow-lg relative "
      style={{
        backgroundImage: 'url("https://images.unsplash.com/photo-1610690063165-b14c5a19e4e4?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D")',
      }}
    >
      <div className="  relative top-24 text-center  ">
        <h1 className="font-bold text-5xl ">Ideas</h1>
        <p className=" text-xl"> Where all our great begins</p>
      </div>
      <div className="mx-auto h-0 w-0 border-r-[-50px] border-b-[323px] border-l-[2042px] border-solid border-r-transparent border-l-transparent border-b-white absolute top-48 right-0" />
      <div className=" mt-60  z-20  relative ">
        <div className="bg-white py-28  flex justify-center ">{children}</div>
      </div>
    </div>
  );
}
