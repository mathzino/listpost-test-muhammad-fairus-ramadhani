"use client";
import { ImgHTMLAttributes, useCallback, useEffect, useRef, useState } from "react";

import clsx from "clsx";

interface IImageLoader extends ImgHTMLAttributes<HTMLImageElement> {
  imageUrl: string;
  imageSmallUrl: string;
}

export default function ImageLoader({ imageSmallUrl, imageUrl, ...props }: IImageLoader) {
  const [loaded, setLoaded] = useState<boolean>(false);
  const imgRef = useRef<HTMLImageElement>(null);
  useEffect(() => {
    imgRef.current?.complete && setLoaded(true);
  }, []);
  const onLoad = useCallback(() => {
    setLoaded(true);
  }, []);
  return (
    <div>
      <div>
        <img loading="lazy" src={imageSmallUrl} className={clsx("opacity-70 blur-sm rounded-lg object-cover w-full h-60 overflow-hidden", loaded && "hidden")} {...props} />
      </div>
      <div>
        <img ref={imgRef} loading="lazy" src={imageUrl} onLoad={onLoad} className={clsx(loaded ? " rounded-lg  object-cover w-full h-60 overflow-hidden" : " opacity-0 w-0 h-0 ")} {...props} />
      </div>
    </div>
  );
}
