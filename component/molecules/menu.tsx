"use client";
import React from "react";
import clsx from "clsx";
import Link from "next/link";
import { usePathname } from "next/navigation";

export type TMenu = {
  label: string;
  value: string;
};

export interface IMenuProps {
  listMenu: TMenu[];
  className?: string;
}

const Menu: React.FC<IMenuProps> = ({ listMenu, className }) => {
  const pathName = usePathname();

  return (
    <div className={clsx(" text-base leading-loose  flex  gap-10 font-bold text-white", className)}>
      {listMenu.map((item, index) => {
        return (
          <Link href={item.value} key={index} className={clsx("", pathName === item.value && " border-b-4 border-white")}>
            {item.label}
          </Link>
        );
      })}
    </div>
  );
};

export default Menu;
