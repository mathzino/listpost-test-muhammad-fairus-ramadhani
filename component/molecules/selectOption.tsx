import React, { SelectHTMLAttributes } from "react";

type Toption = {
  name: string;
  value?: string | number;
};

interface ISelect extends SelectHTMLAttributes<HTMLSelectElement> {
  options: Toption[];
}

export default function SelectOption({ options, name, id, ...props }: ISelect) {
  return (
    <select name={name} id={id} {...props}>
      {options.map((option, i) => {
        return (
          <option key={i} value={option.value || option.name}>
            {option.name}
          </option>
        );
      })}
    </select>
  );
}
